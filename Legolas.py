class Legolas:
    def __str__(self):
        return "Legolas"

    def __init__(self, leben, schaden, reichweite):
        self.leben = leben
        self.schaden = schaden
        self.reichweite = reichweite

    def regen(self, ziel):
        print("Legolas lässt einen Pfeilregen auf den Gegner los!")
        ziel.leben = ziel.leben - self.schaden * 3

    def bogen(self, ziel):
        print("Legolas trifft den Gegner mit einem Pfeil!")
        ziel.leben = ziel.leben - self.schaden