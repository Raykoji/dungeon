class HexenkoenigVonAngmar:
    def __str__(self):
        return "Hexenkönig von Angmar"
    def __init__(self, leben, schaden, reichweite):
        self.leben = leben
        self.schaden = schaden
        self.reichweite = reichweite

    def schwert(self,ziel):
        print("Vorsicht, der Hexenkönig von Angmar führt einen mächtigen Hieb mit dem Schwert aus!")
        ziel.leben = ziel.leben - self.schaden

    def morgenstern(self,ziel):
        print("Vorsicht der Hexenkönig von Angmar holt mit dem Morgenstern aus!")
        ziel.leben = ziel.leben - self.schaden

    def provoziert(self):
        self.provoziert = True