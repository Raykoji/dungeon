#Legolas
import random
random.seed()

gegnerNamensListe = ["Orks", "Uruk-Hai", "Warg"]
heldenNamensListe = ["Gimli", "Aragorn", "Legolas"]

def zufaellligesZielGegnerOhneGimli(heldenListe):
    zziel = random.randint(1,2)

    if zziel == 1:
        ziel = heldenListe.get("Aragorn")

        if ziel is None:
            print("Der Gegener verfehlt seinen Angriff.")

    else:
        ziel = heldenListe.get("Legolas")

        if ziel is None:
            print("Der Gegner verfehlt seinen Angriff.")
    return ziel



def zufaelligesZielGegner(heldenListe):
    zziel = random.randint(1,3)

    if zziel == 1:
        ziel = heldenListe.get("Gimli")

        if ziel is None:
            print("Der Gegner hat seinen Angriff verfehlt")

    if zziel == 2:
        ziel = heldenListe.get("Aragorn")

        if ziel is None:
            print("Der Gegener verfehlt seinen Angriff.")

    else:
        ziel = heldenListe.get("Legolas")

        if ziel is None:
            print("Der Gegner verfehlt seinen Angriff.")
    return ziel

def wuerfelAngriff():
    z = random.randint(1,5)

    if z == 1:
        print("Du hast verfehlt")
        return False

    else:
        print("Der Gegner wurde getroffen!")
        return True

def legolasFaehigkeit(heldenListe, gegnerListe):
    legolas = heldenListe.get("Legolas")
    print("Wähle dein Ziel aus.", gegnerNamensListe[0], "(0)", gegnerNamensListe[1], "(1)", gegnerNamensListe[2], "(2)")
    fziel = int(input())
    ziel = gegnerListe.get(gegnerNamensListe[fziel])
    if ziel is None:
        print("Ist bereits besiegt.")

    else:
        if wuerfelAngriff():
            legolas.regen(ziel)
            print("Der", ziel, "hat noch", ziel.leben, "HP.\n")

def legolasAngriff(heldenListe, gegnerListe):
    legolas = heldenListe.get("Legolas")
    print("Wähle dein Ziel aus.", gegnerNamensListe[0], "(0)", gegnerNamensListe[1], "(1)", gegnerNamensListe[2], "(2)")
    aziel = int(input())
    ziel = gegnerListe.get(gegnerNamensListe[aziel])
    if ziel is None:
        print("Ist bereits besiegt.")

    else:
        if wuerfelAngriff():
            legolas.bogen(ziel)
            print("Der", ziel, "hat noch", ziel.leben, "HP.\n")

#Gimli
def gimliFaehigkeit(heldenListe,gegnerListe):
    gimli = heldenListe.get("Gimli")
    ziel3 = gegnerListe.get("Orks")
    ziel4 = gegnerListe.get("Uruk-Hai")
    ziel5 = gegnerListe.get("Warg")
    ziel6 = gegnerListe.get("Hexenkönig")
    if wuerfelAngriff():
        gimli.durinsStolz(ziel3, ziel4, ziel5,ziel6)

def gimliAngriff(heldenListe, gegnerListe):
    gimli = heldenListe.get("Gimli")
    print("Wähle dein Ziel aus.", gegnerNamensListe[0], "(0)", gegnerNamensListe[1], "(1)", gegnerNamensListe[2], "(2)")
    aziel = int(input())
    ziel = gegnerListe.get(gegnerNamensListe[aziel])
    if ziel is None:
        print("Ist bereits besiegt.")

    else:
        if wuerfelAngriff():
            gimli.axtSchlag(ziel)
            print("Der", ziel, "hat noch", ziel.leben, "HP.\n")


#Aragorn
def aragornFaehigkeit(heldenListe):
    aragorn = heldenListe.get("Aragorn")
    print("Wähle dein Ziel aus.", heldenNamensListe[0], "(0)", heldenNamensListe[1], "(1)", heldenNamensListe[2], "(2)")
    fziel = int(input())
    ziel = heldenListe.get(heldenNamensListe[fziel])
    if wuerfelAngriff():
        aragorn.elessar(ziel)
        print(ziel, "hat nun", ziel.leben, ".\n")

def aragornAngriff(heldenListe, gegnerListe):
    aragorn = heldenListe.get("Aragorn")
    print("Wähle dein Ziel aus.", gegnerNamensListe[0], "(0)", gegnerNamensListe[1], "(1)", gegnerNamensListe[2], "(2)")
    aziel = int(input())
    ziel = gegnerListe.get(gegnerNamensListe[aziel])
    if ziel is None:
        print("Ist bereits besiegt.")

    else:
        if wuerfelAngriff():
            aragorn.schwert(ziel)
            print("Der", ziel, "hat noch", ziel.leben, "HP.\n")


#Ork
def orkAngriff(heldenListe, gegnerListe):
    ork = gegnerListe.get("Orks")
    print("Der Ork schießt einen Pfeil ab!")
    if ork.provoziert:
        ziel = heldenListe.get("Gimli")
        if ziel is None:
            ziel = zufaellligesZielGegnerOhneGimli(heldenListe)
        ork.bogen(ziel)
        print("Der Ork hat", ziel, "angegriffen. Er hat noch", ziel.leben, "HP.\n")
    else:
        ziel = zufaelligesZielGegner(heldenListe)
        ork.bogen(ziel)
        print("Der Ork hat", ziel, "angegriffen. Er hat noch", ziel.leben, "HP.\n")
#Uruk-Hai
def urukHaiAngriff(heldenListe, gegnerListe):
    urukHai = gegnerListe.get("Uruk-Hai")
    print("Der Uruk-Hai greift an!")
    if urukHai.provoziert:
        ziel = heldenListe.get("Gimli")
        if ziel is None:
            ziel = zufaellligesZielGegnerOhneGimli(heldenListe)
        urukHai.schwert(ziel)
        print("Der Uruk-Hai hat", ziel, "angegriffen. Er hat noch", ziel.leben, "HP.\n")
    else:
        ziel = zufaelligesZielGegner(heldenListe)
        urukHai.schwert(ziel)
        print("Der Uruk-Hai hat", ziel, "angegriffen. Er hat noch", ziel.leben, "HP.\n")

#Warg
def wargAngriff(heldenListe, gegnerListe):
    warg = gegnerListe.get("Warg")
    print("Der Warg fällt einen Verbündeten an!")
    if warg.provoziert:
        ziel = heldenListe.get("Gimli")
        if ziel is None:
            ziel = zufaellligesZielGegnerOhneGimli(heldenListe)
        warg.anfallen(ziel)
        print("Der Warg hat", ziel, "angefallen. Er hat noch", ziel.leben, "HP.\n")
    else:
        ziel = zufaelligesZielGegner(heldenListe)
        warg.anfallen(ziel)
        print("Der Warg hat", ziel, "angefallen. Er hat noch", ziel.leben, "HP.\n")

#Hexenkönig von Angmar
def hexenkoenigAngriff(heldenListe, gegnerListe):
    hexenkoenig = gegnerListe.get("Hexenkönig")
    if hexenkoenig.provoziert and heldenListe.get("Gimli") is not None:
        ziel = heldenListe.get("Gimli")
        if ziel is None:
            ziel = zufaellligesZielGegnerOhneGimli(heldenListe)
        hexenkoenig.schwert(ziel)
        print("Der Hexenkönig von Angmar hat", ziel, "angegriffen. Er hat noch", ziel.leben, "HP.\n")
    else:
        ziel = heldenListe.get("Legolas")
        if ziel is not None:
            hexenkoenig.schwert(ziel)
            print("Der Hexenkönig von Angmar hat", ziel, "angegriffen. Er hat noch", ziel.leben, "HP.\n")
        else:
            ziel = zufaelligesZielGegner(heldenListe)
            hexenkoenig.schwert(ziel)

def hexenkoenigMorgenstern(heldenListe, gegnerListe):
    hexenkoenig = gegnerListe.get("Hexenkönig")
    if hexenkoenig.provoziert and heldenListe.get("Gimli") is not None:
        ziel = heldenListe.get("Gimli")
        if ziel is None:
            ziel = zufaellligesZielGegnerOhneGimli(heldenListe)
        hexenkoenig.morgenstern(ziel)
        print("Der Hexenkönig von Angmar hat", ziel, "angegriffen. Er hat noch", ziel.leben, "HP.\n")
    else:
        ziel = heldenListe.get("Aragorn")
        if ziel is not None:
            hexenkoenig.morgenstern(ziel)
            print("Der Hexenkönig von Angmar hat", ziel, "angegriffen. Er hat noch", ziel.leben, "HP.\n")
        else:
            ziel = zufaelligesZielGegner(heldenListe)
            hexenkoenig.schwert(ziel)