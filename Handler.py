import random
random.seed()

from KampfHandler import legolasFaehigkeit, legolasAngriff, orkAngriff, aragornFaehigkeit, aragornAngriff, \
    urukHaiAngriff, gimliFaehigkeit, gimliAngriff, wargAngriff, hexenkoenigAngriff, hexenkoenigMorgenstern


def wuerfelZug(heldenListe, gegnerListe):
    zahl = random.randint(1,2)
    if zahl == 1:
        print("Die Helden haben Gegner entdeckt! Der Kampf beginnt.")
        print("Die Kampfreihenfolge richtet sich nach der Reichweite der Einheiten. Die Einheit mit der größten Einheit beginnt\n")
        amLeben = pruefeAmLebenSpieler(heldenListe)
        while amLeben:
            if heldenListe.get("Legolas") and heldenListe.get("Legolas").leben > 0:
                print("Möchtest du Legolas´s Fähigkeit einsetzen? Ja(1), Nein(2)")
                faehigkeit = input()
                if faehigkeit == "Ja" or faehigkeit == "1":
                    legolasFaehigkeit(heldenListe, gegnerListe)
                else:
                    print("Legolas führt einen normalen Angriff aus.")
                    legolasAngriff(heldenListe, gegnerListe)

            else:
                try:
                    print("Legolas ist schwer verletzt und kann nicht mehr angreifen.")
                    del heldenListe["Legolas"]

                except:
                    print("Legolas ist schwer verletzt.")
            if not (pruefeAmLebenGegner(gegnerListe) & pruefeAmLebenSpieler(heldenListe)):
                break

            if gegnerListe.get("Orks") and gegnerListe.get("Orks").leben > 0:
                orkAngriff(heldenListe, gegnerListe)

            else:
                try:
                    del gegnerListe["Orks"]
                except:
                    print("Ork wurde besiegt.")

            if heldenListe.get("Aragorn") and heldenListe.get("Aragorn").leben > 0:
                print("Möchtest du Aragorn´s Fähigkeit einsetzen? Ja(1), Nein(2)")
                faehigkeit = input()
                if faehigkeit == "Ja" or faehigkeit == "1":
                    aragornFaehigkeit(heldenListe)
                else:
                    print("Aragorn führt einen normalen Angriff aus.")
                    aragornAngriff(heldenListe, gegnerListe)

            else:
                try:
                    print("Aragorn ist schwer verletzt und kann nicht mehr angreifen.")
                    del heldenListe["Aragorn"]

                except:
                    print("Aragorn ist schwer verletzt.")

            if not (pruefeAmLebenGegner(gegnerListe) & pruefeAmLebenSpieler(heldenListe)):
                break

            if gegnerListe.get("Uruk-Hai") and gegnerListe.get("Uruk-Hai").leben > 0:
                urukHaiAngriff(heldenListe, gegnerListe)

            else:
                try:
                    del gegnerListe["Uruk-Hai"]
                except:
                    print("Uruk-Hai wurde besiegt.")

            if heldenListe.get("Gimli") and heldenListe.get("Gimli").leben > 0:
                print("Möchtest du Gimli´s Fähigkeit einsetzen? Ja(1), Nein(2)")
                faehigkeit = input()
                if faehigkeit == "Ja" or faehigkeit == "1":
                    gimliFaehigkeit(heldenListe, gegnerListe)
                else:
                    print("Gimli führt einen normalen Angriff aus.")
                    gimliAngriff(heldenListe, gegnerListe)

            else:
                try:
                    print("Gimli ist schwer verletzt und kann nicht mehr angreifen.")
                    del heldenListe["Gimli"]

                except:
                    print("Gimli ist schwer verletzt.")

            if gegnerListe.get("Warg") and gegnerListe.get("Warg").leben > 0:
                wargAngriff(heldenListe, gegnerListe)

            else:
                try:
                    del gegnerListe["Warg"]
                except:
                    print("Warg wurde besiegt.")


            amLeben = pruefeAmLebenSpieler(heldenListe) & pruefeAmLebenGegner(gegnerListe)


#Gegner greift zuerst an
    else:
        print("Oh nein, die Helden sind in einen Hinterhalt geraten!")
        if gegnerListe.get("Orks") and gegnerListe.get("Orks").leben > 0:
            orkAngriff(heldenListe, gegnerListe)

        else:
            try:
                del gegnerListe["Orks"]
            except:
                print("Ork wurde besiegt.")


        print("Die Kampfreihenfolge richtet sich nach der Reichweite der Einheiten. Die Einheit mit der größten Einheit beginnt\n")
        amLeben = pruefeAmLebenSpieler(heldenListe)
        while amLeben:
            if heldenListe.get("Legolas") and heldenListe.get("Legolas").leben > 0:
                print("Möchtest du Legolas´s Fähigkeit einsetzen? Ja(1), Nein(2)")
                faehigkeit = input()
                if faehigkeit == "Ja" or faehigkeit == "1":
                    legolasFaehigkeit(heldenListe, gegnerListe)
                else:
                    print("Legolas führt einen normalen Angriff aus.")
                    legolasAngriff(heldenListe, gegnerListe)

            else:
                try:
                    print("Legolas ist schwer verletzt und kann nicht mehr angreifen.")
                    del heldenListe["Legolas"]

                except:
                    print("Legolas ist schwer verletzt.")

            if not (pruefeAmLebenGegner(gegnerListe) and pruefeAmLebenSpieler(heldenListe)):
                break

            if gegnerListe.get("Uruk-Hai") and gegnerListe.get("Uruk-Hai").leben > 0:
                urukHaiAngriff(heldenListe, gegnerListe)

            else:
                try:
                    del gegnerListe["Uruk-Hai"]
                except:
                    print("Uruk-Hai wurde besiegt.")

            if heldenListe.get("Aragorn") and heldenListe.get("Aragorn").leben > 0:
                print("Möchtest du Aragorn´s Fähigkeit einsetzen? Ja(1), Nein(2)")
                faehigkeit = input()
                if faehigkeit == "Ja" or faehigkeit == "1":
                    aragornFaehigkeit(heldenListe)
                else:
                    print("Aragorn führt einen normalen Angriff aus.")
                    aragornAngriff(heldenListe,gegnerListe)

            else:
                try:
                    print("Aragorn ist schwer verletzt und kann nicht mehr angreifen.")
                    del heldenListe["Aragorn"]

                except:
                    print("Aragorn ist schwer verletzt.")

            if not (pruefeAmLebenGegner(gegnerListe) and pruefeAmLebenSpieler(heldenListe)):
                break

            if gegnerListe.get("Warg") and gegnerListe.get("Warg").leben > 0:
                wargAngriff(heldenListe, gegnerListe)

            else:
                try:
                    del gegnerListe["Warg"]
                except:
                    print("Warg wurde besiegt.")


            if heldenListe.get("Gimli") and heldenListe.get("Gimli").leben > 0:
                print("Möchtest du Gimli´s Fähigkeit einsetzen? Ja(1), Nein(2)")
                faehigkeit = input()
                if faehigkeit == "Ja" or faehigkeit == "1":
                    gimliFaehigkeit(heldenListe, gegnerListe)
                else:
                    print("Gimli führt einen normalen Angriff aus.")
                    gimliAngriff(heldenListe, gegnerListe)

            else:
                try:
                    print("Gimli ist schwer verletzt und kann nicht mehr angreifen.")
                    del heldenListe["Gimli"]

                except:
                    print("Gimli ist schwer verletzt.")

            amLeben = pruefeAmLebenSpieler(heldenListe) and pruefeAmLebenGegner(gegnerListe)

def bossKampf(heldenListe, gegnerListe):
    print("Die Helden haben den Herrscher von Minas Morgul gefunden! Der Kampf mit dem Hexenkönig von Angmar beginnt.")
    amLeben = pruefeAmLebenSpieler(heldenListe)
    while amLeben:
        if heldenListe.get("Legolas") and heldenListe.get("Legolas").leben > 0:
            print("Möchtest du Legolas´s Fähigkeit einsetzen? Ja(1), Nein(2)")
            faehigkeit = input()
            if faehigkeit == "Ja" or faehigkeit == "1":
                heldenListe.get("Legolas").regen(gegnerListe.get("Hexenkönig"))
                print("Der", gegnerListe.get("Hexenkönig"), "hat noch", gegnerListe.get("Hexenkönig").leben, "HP.\n")

            else:
                print("Legolas führt einen normalen Angriff aus.")
                heldenListe.get("Legolas").bogen(gegnerListe.get("Hexenkönig"))
                print("Der", gegnerListe.get("Hexenkönig"), "hat noch", gegnerListe.get("Hexenkönig").leben, "HP.\n")

        else:
            try:
                print("Legolas ist schwer verletzt und kann nicht mehr angreifen.")
                del heldenListe["Legolas"]

            except:
                print("Legolas ist schwer verletzt.")

        if not (pruefeAmLebenBoss(gegnerListe) and pruefeAmLebenSpieler(heldenListe)):
            break

        if gegnerListe.get("Hexenkönig") and gegnerListe.get("Hexenkönig").leben > 0:
            hexenkoenigAngriff(heldenListe, gegnerListe)


        if heldenListe.get("Aragorn") and heldenListe.get("Aragorn").leben > 0:
            print("Möchtest du Aragorn´s Fähigkeit einsetzen? Ja(1), Nein(2)")
            faehigkeit = input()
            if faehigkeit == "Ja" or faehigkeit == "1":
                aragornFaehigkeit(heldenListe)
            else:
                print("Aragorn führt einen normalen Angriff aus.")
                heldenListe.get("Aragorn").schwert(gegnerListe.get("Hexenkönig"))
                print("Der", gegnerListe.get("Hexenkönig"), "hat noch", gegnerListe.get("Hexenkönig").leben, "HP.\n")

        else:
            try:
                print("Aragorn ist schwer verletzt und kann nicht mehr angreifen.")
                del heldenListe["Aragorn"]

            except:
                print("Aragorn ist schwer verletzt.")

        if not (pruefeAmLebenBoss(gegnerListe) and pruefeAmLebenSpieler(heldenListe)):
            break

        if gegnerListe.get("Hexenkönig") and gegnerListe.get("Hexenkönig").leben > 0:
            hexenkoenigAngriff(heldenListe, gegnerListe)


        if heldenListe.get("Gimli") and heldenListe.get("Gimli").leben > 0:
            print("Möchtest du Gimli´s Fähigkeit einsetzen? Ja(1), Nein(2)")
            faehigkeit = input()
            if faehigkeit == "Ja" or faehigkeit == "1":
                gimliFaehigkeit(heldenListe, gegnerListe)
            else:
                print("Gimli führt einen normalen Angriff aus.")
                heldenListe.get("Gimli").axtSchlag(gegnerListe.get("Hexenkönig"))
                print("Der", gegnerListe.get("Hexenkönig"), "hat noch", gegnerListe.get("Hexenkönig").leben, "HP.\n")

        else:
            try:
                print("Gimli ist schwer verletzt und kann nicht mehr angreifen.")
                del heldenListe["Gimli"]

            except:
                print("Gimli ist schwer verletzt.")

        if gegnerListe.get("Hexenkönig") and gegnerListe.get("Hexenkönig").leben > 0:
            hexenkoenigMorgenstern(heldenListe, gegnerListe)


        amLeben = pruefeAmLebenSpieler(heldenListe) and pruefeAmLebenBoss(gegnerListe)


def pruefeAmLebenBoss(gegnerListe):
    return gegnerListe.get("Hexenkönig").leben > 0


def pruefeAmLebenGegner(gegnerListe):
    lebendigO = gegnerListe.get("Orks") is not None
    lebendigU = gegnerListe.get("Uruk-Hai") is not None
    lebendigW = gegnerListe.get("Warg") is not None
    lebendig = lebendigO | lebendigU | lebendigW
    return lebendig


def pruefeAmLebenSpieler(heldenListe):
    lebendigG = heldenListe.get("Gimli") is not None
    lebendigA = heldenListe.get("Aragorn") is not None
    lebendigL = heldenListe.get("Legolas") is not None
    lebendig = lebendigG | lebendigA | lebendigL
    return lebendig
