from Gimli import Gimli
from Aragorn import Aragorn
from Handler import wuerfelZug, pruefeAmLebenSpieler, pruefeAmLebenGegner, bossKampf
from Legolas import Legolas
from Orks import Orks
from Uruk_Hai import UrukHai
from Warg import Warg
from Hexenkönig_von_Angmar import HexenkoenigVonAngmar

#Stats Helden
gimli = Gimli(3000, 100, 1)
aragorn = Aragorn(1000, 50, 2)
legolas = Legolas(2000, 150, 3)
heldenListe = {"Gimli" : gimli, "Aragorn" : aragorn, "Legolas" : legolas}

#Stats Gegner
orks = Orks(500, 1000, 3)
uruk_hai = UrukHai(1000, 100, 2)
warg = Warg(500, 1500, 1)
hexenkoenig = HexenkoenigVonAngmar(3000, 250, 2)
gegnerListe = {"Orks" : orks, "Uruk-Hai" : uruk_hai, "Warg" : warg, "Hexenkönig" : hexenkoenig}

#Dungeon Beginn
print("Legolas, Gimli und Aragorn betreten Minas Morgul um den Hexenkönig von Angmar aufzuhalten."
      "Hilf ihnen bei diesem Unterfangen!")
print("Du kannst dir jetzt Fähigkeiten und Stats in der 'Übersicht' ansehen.")
print("Möchtest du dir die Übersicht ansehen?", "Ja = 1, Nein = 2")
startAntwort = int(input())

if startAntwort == 1:
    print("Gimli hat", gimli.leben, "HP,", gimli.schaden, "Angriffschaden und eine Reichweite von", gimli.reichweite)
    print("Er hat die Fähigkeit 'Durins Stolz' mit der er die Aufmerksamkeit der Gegner auf sich längt.\n")

    print("Aragorn hat", aragorn.leben, "HP,", aragorn.schaden, "Angriffschaden und eine Reichweite von", aragorn.reichweite)
    print("Er hat die Fähigkeit 'Elessar' mit der er einen ausgewählten Verbündeten heilen kann.\n")

    print("Legolas hat", legolas.leben, "HP,", legolas.schaden, "Angriffschaden und eine Reichweite von", legolas.reichweite)
    print("Er hat die Fähigkeit 'Galadhrims Regen' mit dem er mehrere Pfeile auf einen Gegner niedergehen lassen kann.\n")

    print("Bist du bereit?")
    statAntwort = input()
    if statAntwort == "karl":
        print("Ja Jakob. Du mich auch...")
else:
    print("Mach dich bereit!")

#Kampf Gegner
wuerfelZug(heldenListe, gegnerListe)
if pruefeAmLebenSpieler(heldenListe):
    print("Die Gegener wurden besiegt und schreiten weiter voran.")
    #Boss
    bossKampf(heldenListe, gegnerListe)

    if pruefeAmLebenSpieler(heldenListe):
        print("Eine rote Sonne geht auf. In der Nacht ist blut vergossen worden. Doch durch die Heldentaten eines Zwerges, "
        "eines Elben und eines Dunedain ist die Welt von Mittelerde nun etwas sicherer geworden.")

    else:
        print("Unsere Helden sind gefallen und Mittelerde muss ohne sie den Mächten des Dunkeln gegenüber treten.")

else:
    print("Ein trauriger Tag in der Geschichte Mittelerdes. Die drei Helden wurden von den Feinden besiegt.")

