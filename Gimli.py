class Gimli:

    def __str__(self):
        return "Gimli"

    def __init__(self, leben, schaden, reichweite):
        self.leben = leben
        self.schaden = schaden
        self.reichweite = reichweite

    def durinsStolz(self,ziel3, ziel4, ziel5, ziel6):
        print("Gimli zieht die Aufmerksamkeit der Gegner auf sich!")
        ziel3.provoziert = True
        ziel4.provoziert = True
        ziel5.provoziert = True
        ziel6.provoziert = True

    def axtSchlag(self,ziel):
        print("Gimli verpasst dem Gegner einen Axthieb!")
        ziel.leben = ziel.leben - self.schaden