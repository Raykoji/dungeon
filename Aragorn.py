class Aragorn:
    def __str__(self):
        return "Aragorn"

    def __init__(self, leben, schaden, reichweite):
        self.leben = leben
        self.schaden = schaden
        self.reichweite = reichweite

    def elessar(self, ziel):
        print("Aragorn hat einen Verbündeten geheilt!")
        ziel.leben = ziel.leben +100

    def schwert(self, ziel):
        print("Aragorn trifft den Gegner mit einem Schwerthieb!")
        ziel.leben = ziel.leben -self.schaden