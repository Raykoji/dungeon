class Warg:
    def __str__(self):
        return "Warg"

    def __init__(self, leben, schaden, reichweite):
        self.leben = leben
        self.schaden = schaden
        self.reichweite = reichweite

    def anfallen(self,ziel):
        ziel.leben = ziel.leben - self.schaden

    def provoziert(self):
        self.provoziert = True